/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionClose;
    QWidget *centralwidget;
    QLabel *labelTop;
    QProgressBar *progressBar;
    QFrame *line;
    QLabel *label;
    QPushButton *CCW_rotation;
    QPushButton *CW_rotation;
    QSlider *color_bar;
    QRadioButton *MIP_trans;
    QRadioButton *FirstHit_trans;
    QRadioButton *alpha_trans;
    QRadioButton *Grad_trans;
    QPushButton *pushMinusZ;
    QPushButton *pushPlusY;
    QPushButton *pushPlusX;
    QPushButton *pushPlusZ;
    QPushButton *pushMinusY;
    QPushButton *pushMinusX;
    QMenuBar *menubar;
    QMenu *menuFile;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(769, 680);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QStringLiteral("actionClose"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        labelTop = new QLabel(centralwidget);
        labelTop->setObjectName(QStringLiteral("labelTop"));
        labelTop->setGeometry(QRect(10, 20, 1001, 16));
        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setEnabled(false);
        progressBar->setGeometry(QRect(630, 20, 121, 23));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);
        line = new QFrame(centralwidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(0, 49, 781, 51));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setEnabled(true);
        label->setGeometry(QRect(0, 90, 771, 391));
        label->setAutoFillBackground(false);
        label->setFrameShape(QFrame::Box);
        label->setFrameShadow(QFrame::Sunken);
        label->setScaledContents(false);
        label->setAlignment(Qt::AlignCenter);
        label->setProperty("value", QVariant(0));
        label->setProperty("textVisible", QVariant(false));
        CCW_rotation = new QPushButton(centralwidget);
        CCW_rotation->setObjectName(QStringLiteral("CCW_rotation"));
        CCW_rotation->setGeometry(QRect(310, 530, 161, 41));
        QIcon icon;
        QString iconThemeName = QStringLiteral("ccw");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QStringLiteral("CCW.png"), QSize(), QIcon::Normal, QIcon::Off);
            icon.addFile(QStringLiteral("CCW.png"), QSize(), QIcon::Normal, QIcon::On);
        }
        CCW_rotation->setIcon(icon);
        CCW_rotation->setIconSize(QSize(200, 35));
        CW_rotation = new QPushButton(centralwidget);
        CW_rotation->setObjectName(QStringLiteral("CW_rotation"));
        CW_rotation->setGeometry(QRect(310, 580, 161, 41));
        QIcon icon1;
        iconThemeName = QStringLiteral("cw");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QStringLiteral("CW.png"), QSize(), QIcon::Normal, QIcon::Off);
            icon1.addFile(QStringLiteral("CW.png"), QSize(), QIcon::Normal, QIcon::On);
        }
        CW_rotation->setIcon(icon1);
        CW_rotation->setIconSize(QSize(200, 40));
        color_bar = new QSlider(centralwidget);
        color_bar->setObjectName(QStringLiteral("color_bar"));
        color_bar->setGeometry(QRect(10, 490, 751, 20));
        color_bar->setOrientation(Qt::Horizontal);
        MIP_trans = new QRadioButton(centralwidget);
        MIP_trans->setObjectName(QStringLiteral("MIP_trans"));
        MIP_trans->setEnabled(true);
        MIP_trans->setGeometry(QRect(10, 530, 301, 17));
        MIP_trans->setChecked(true);
        FirstHit_trans = new QRadioButton(centralwidget);
        FirstHit_trans->setObjectName(QStringLiteral("FirstHit_trans"));
        FirstHit_trans->setGeometry(QRect(10, 560, 311, 17));
        alpha_trans = new QRadioButton(centralwidget);
        alpha_trans->setObjectName(QStringLiteral("alpha_trans"));
        alpha_trans->setGeometry(QRect(10, 590, 281, 17));
        Grad_trans = new QRadioButton(centralwidget);
        Grad_trans->setObjectName(QStringLiteral("Grad_trans"));
        Grad_trans->setGeometry(QRect(10, 620, 381, 17));
        pushMinusZ = new QPushButton(centralwidget);
        pushMinusZ->setObjectName(QStringLiteral("pushMinusZ"));
        pushMinusZ->setGeometry(QRect(660, 530, 91, 31));
        pushPlusY = new QPushButton(centralwidget);
        pushPlusY->setObjectName(QStringLiteral("pushPlusY"));
        pushPlusY->setGeometry(QRect(550, 570, 91, 31));
        pushPlusX = new QPushButton(centralwidget);
        pushPlusX->setObjectName(QStringLiteral("pushPlusX"));
        pushPlusX->setGeometry(QRect(550, 610, 91, 31));
        pushPlusZ = new QPushButton(centralwidget);
        pushPlusZ->setObjectName(QStringLiteral("pushPlusZ"));
        pushPlusZ->setGeometry(QRect(550, 530, 91, 31));
        pushMinusY = new QPushButton(centralwidget);
        pushMinusY->setObjectName(QStringLiteral("pushMinusY"));
        pushMinusY->setGeometry(QRect(660, 570, 91, 31));
        pushMinusX = new QPushButton(centralwidget);
        pushMinusX->setObjectName(QStringLiteral("pushMinusX"));
        pushMinusX->setGeometry(QRect(660, 610, 91, 31));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 769, 21));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menubar);

        menubar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addSeparator();
        menuFile->addAction(actionClose);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Visualisierung 1", 0));
        actionOpen->setText(QApplication::translate("MainWindow", "Open ...", 0));
        actionClose->setText(QApplication::translate("MainWindow", "Close", 0));
        labelTop->setText(QApplication::translate("MainWindow", "No data loaded", 0));
        CCW_rotation->setText(QString());
        CW_rotation->setText(QString());
        MIP_trans->setText(QApplication::translate("MainWindow", "MIP ", 0));
        FirstHit_trans->setText(QApplication::translate("MainWindow", "First Hit ", 0));
        alpha_trans->setText(QApplication::translate("MainWindow", "Alpha Compositing ", 0));
        Grad_trans->setText(QApplication::translate("MainWindow", "Diffuses Gradientenshading", 0));
        pushMinusZ->setText(QApplication::translate("MainWindow", "View Bottom", 0));
        pushPlusY->setText(QApplication::translate("MainWindow", "View Left", 0));
        pushPlusX->setText(QApplication::translate("MainWindow", "View Front", 0));
        pushPlusZ->setText(QApplication::translate("MainWindow", "View Top", 0));
        pushMinusY->setText(QApplication::translate("MainWindow", "View Right", 0));
        pushMinusX->setText(QApplication::translate("MainWindow", "View Back", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
